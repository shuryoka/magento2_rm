#!/bin/sh

if [ ! -d "/run/mysqld" ]; then
	mkdir -p /run/mysqld
	chown -R mysql:mysql /run/mysqld
fi

if [ -d /var/lib/mysql/mysql ]; then
	echo '[i] MySQL directory already present, skipping creation'
else
	echo "[i] MySQL data directory not found, creating initial DBs"

	chown -R mysql:mysql /var/lib/mysql

	# init database
	echo 'Initializing database'
	mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql > /dev/null
	echo 'Database initialized'

	echo "[i] MySql root password: $2"

	# create temp file
	tfile=`mktemp`
	if [ ! -f "$tfile" ]; then
	    return 1
	fi

	# save sql
	echo "[i] Create temp file: $tfile"
	cat << EOF > $tfile
USE mysql;
FLUSH PRIVILEGES;
DELETE FROM mysql.user;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY '$2' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '$2' WITH GRANT OPTION;
EOF

	# Create new database
	if [ "$3" != "" ]; then
		echo "[i] Creating database: $3"
		echo "CREATE DATABASE IF NOT EXISTS \`$3\` CHARACTER SET utf8 COLLATE utf8_general_ci;" >> $tfile

		# set new User and Password
		if [ "$1" != "" ] && [ "$2" != "" ]; then
            echo "[i] Creating user: $1 with password $2"
            echo "GRANT ALL ON \`$3\`.* to '$1'@'%' IDENTIFIED BY '$2';" >> $tfile
		fi
	else
		# don`t need to create new database, Set new User to control all database.
		if [ "$1" != "" ] && [ "$2" != "" ]; then
            echo "[i] Creating user: $1 with password $2"
            echo "GRANT ALL ON *.* to '$1'@'%' IDENTIFIED BY '$2';" >> $tfile
		fi
	fi

	echo 'FLUSH PRIVILEGES;' >> $tfile

	# run sql in tempfile
	echo "[i] run tempfile: $tfile"
	mysqld --user=$1 --bootstrap --verbose=0 < $tfile
	rm -f $tfile
fi

echo "[i] Sleeping 10 sec"
sleep 10

echo '[i] start running mysqld'
exec mysqld --user=mysql --console &
#sleep 10
