FROM mariadb:10.4

ENV MAGENTO_ROOT "/etc/magento/"
ENV MAGENTO_VERSION "2.4.2"
ENV MYSQL_ROOT_PASSWORD "root"
ENV MARDIADB_ROOT_PASSWORD "root"
ENV DB_USER "root"
ENV DB_PASS "root"
ENV DB_HOST "127.0.0.1"
ENV DB_NAME "magento"

#COPY ./docker/scripts/mysql_install.sh /usr/local/bin/mysql_install.sh
#COPY ./docker/scripts/mysql_start.sh /usr/local/bin/mysql_start.sh
COPY ./docker/templates/php.ini /etc/php/7.4/cli/php.ini

# update packages and install software
RUN echo "start installation process" && \
    export MARIADB_ROOT_PASSWORD="${DB_PASS}" && \
    export MYSQL_ROOT_PASSWORD="${DB_PASS}" && \
    apt-get update && apt-get upgrade -y && \
    apt install -y software-properties-common && \
    add-apt-repository ppa:ondrej/php && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
        ca-certificates \
        coreutils \
        openssh-client \
        curl \
        git \
        tar \
        bash \
        redis-server \
        rsync \
        unzip \
        php7.4-opcache \
        php7.4-cli \
        php7.4-gd \
        php7.4-ctype \
        php7.4-json \
        php7.4-bcmath \
        php7.4-mysqli \
        php7.4-mysql \
        php7.4-mcrypt \
        php7.4-mbstring \
        php7.4-curl \
        php7.4-iconv \
        php7.4-intl \
        php7.4-dom \
        php7.4-xml \
        php7.4-xmlwriter \
        php7.4-simplexml \
        php7.4-soap \
        php7.4-sockets \
        php7.4-json \
        php7.4-tokenizer \
        php7.4-xsl \
        php7.4-zip \
        php7.4-phar \
        libfreetype-dev \
        libpng-dev \
        libjpeg-dev \
        libwebp-dev && \
#    curl -fsSL https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add - && \
#    echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-7.x.list && \
#    apt update && apt install -y elasticsearch && \
    rm -rf /var/lib/apt/lists/* && \
    chmod -R 770 /var/lib/mysql && chgrp -R mysql /var/lib/mysql && \
    mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql > /dev/null && \
    echo "Download Composer" && php -r 'echo file_get_contents("https://getcomposer.org/installer");' | php -- --install-dir=/usr/local/bin --filename=composer && \
    echo "Download magerun2" && php -r 'file_put_contents("n98-magerun2.phar", file_get_contents("https://files.magerun.net/n98-magerun2.phar"));' && \
    chmod +x n98-magerun2.phar && \
    mv n98-magerun2.phar /usr/local/bin/magerun2 && \
    /usr/local/bin/magerun2 self-update && \
    /usr/local/bin/magerun2 --version && \
    composer config --global home && \
    curl -LO https://deployer.org/deployer.phar && \
    mv deployer.phar /usr/local/bin/dep && \
    chmod a+x /usr/local/bin/dep && \
    composer self-update 2.1.14

    # pin composer to this version because of an issues with 2.2: https://github.com/magento/magento2/issues/34831

RUN curl elasticsearch:9200

# create Magento Project Structure (by using public availabe Magento keys instead of hosting own ones in the internet)
RUN echo "Download Magento Credentials" && \
    curl https://raw.githubusercontent.com/alexcheng1982/docker-magento2/master/auth.json -sSo /root/.config/composer/auth.json && \
    mkdir -p ${MAGENTO_ROOT} && \
    cd ${MAGENTO_ROOT} && \
    composer create-project --prefer-dist --repository-url="https://repo.magento.com/" magento/project-community-edition=${MAGENTO_VERSION} . && \
    composer clearcache && \
    cd ${MAGENTO_ROOT} && \
    chmod a+x bin/magento

RUN mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql > /dev/null && ( mysqld --user=mysql --console & ) && sleep 15 && \
    mysql -u${DB_USER} -p${DB_PASS} -e "CREATE DATABASE IF NOT EXISTS ${DB_NAME}" && \
#    chown -R elasticsearch:elasticsearch /etc/default/elasticsearch && \
#    export ES_JAVA_OPTS="-Xms512m -Xmx512m" && /etc/init.d/elasticsearch start && \
    cd ${MAGENTO_ROOT} && \
    php bin/magento setup:install \
        --base-url="http://magento2.localhost" \
        --db-name="${DB_NAME}" \
        --db-user="${DB_USER}" \
        --db-password="${DB_PASS}" \    
        --search-engine="elasticsearch7" \
        --elasticsearch-host="elasticsearch" \
        --elasticsearch-port=9200 \
        --elasticsearch-index-prefix=magento \
        --elasticsearch-timeout=15 \
        --admin-firstname="Admin" \
        --admin-lastname="User" \
        --admin-email="christoph.frenes@roastmarket.de" \
        --admin-user="admin.user" \
        --admin-password="admin1234" \
        --language="de_DE" \
        --currency="EUR" \
        --timezone="Europe/Berlin" \
        --use-rewrites="1" \
        --session-save=files \
        --backend-frontname="backend" && \
#    #magerun2 db:dump -c gzip ~/magento_dump.sql && \
    cp ${MAGENTO_ROOT}app/etc/env.php ~/env.php && \
    rm -Rf ${MAGENTO_ROOT}

# prepare SSH
RUN mkdir -p ~/.ssh && eval $(ssh-agent -s) && \
    if [ -f /.dockerenv ]; then echo "Host *\n\tStrictHostKeyChecking no\n\n" > /root/.ssh/config; fi
